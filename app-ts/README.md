## Установка
* Создайте новый проект с помощью
  * ### `npx create-react-app my-app --typescript` 
    or
    ### `yarn create react-app my-app --typescript`

* Скопируйте файлы 
  * ## `.prettierrc`
    и 
  *  ## `.eslintrc` 
  в директорию с вашим проектом
* Откройте в VScode Параметры нажав Ctr+
* Нажмите на значек {} в правом верхнем углу окна VScode
* Скопируйте настройки из файла vc_code_settings.json и добавте в ваш файл настроек.
* Сохраните изменения Ctrl+S
* Откройте файл package.json и скопируйте секцию devDependencies в конфигурационный файл вашего проекта.

* Запустите команду в терминале:

  * ### `npm i`
    или
  * ### `yarn`
* Перезапустите редактор VScode.

Обратите внимания будет использована версия TypeScript 3.5.3. По умолчанию в пакете react-app установлена TypeScript 3.6.2. С новой версией линтер работает не корректно.

## Документация по используемым пакетам
[TypeScript ESLint](https://github.com/typescript-eslint/typescript-eslin)


[TypeScript ESLint Parser](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/parser)

[TypeScript ESLint Plugin](https://github.com/typescript-eslint/typescript-eslint/tree/master/packages/eslint-plugin)

[Eslint-config-prettier](https://github.com/prettier/eslint-config-prettier)

[Eslint-plugin-prettier](https://github.com/prettier/eslint-plugin-prettier)

[Prettier](https://github.com/prettier/prettier)

[Husky](https://github.com/typicode/husky)

[Lint-staged](https://github.com/okonet/lint-staged)

[Настройка ESLint, Prettier, pre-commit hook ](https://maxpfrontend.ru/vebinary/nastroyka-eslint-prettier-pre-commit-hook-create-react-app-visual-studio-code/)

[Configuring ESLint on a TypeScript project](https://javascriptplayground.com/typescript-eslint/)

[ESLint](https://eslint.org/docs/user-guide/getting-started)


